<?php

declare(strict_types=1);

namespace Crux\ZeroFeatureWordPressPlugin;

/*
   Plugin Name: Zero Feature WordPress Plugin Composer Package
   Plugin URI: http://sebrink.de/
   description: This plugin does exactly nothing. It is for demonstrating Packagist and Composer usage in conjunction with WordPress only.",
   Version: 0.0.1
   Author: Dirk Lüsebrink
   Author URI: http://sebrink.de/
   License: MIT
*/
defined('ABSPATH') or die('script kiddies go home!');

// autoloading for composer dependencies
require_once __DIR__ . '/vendor/autoload.php';
